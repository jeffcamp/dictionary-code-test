package com.jeffanddev.android.dictionarycodetest.search

import android.app.Application
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import com.jeffanddev.android.dictionarycodetest.data.preferences.PrefsManager
import com.jeffanddev.android.dictionarycodetest.data.repository.FakeDataRepository
import com.jeffanddev.android.dictionarycodetest.ext.getOrAwaitValue
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SearchViewModelTest {

    private lateinit var viewModel: SearchViewModel
    private lateinit var fakeRepository: FakeDataRepository

    // runs each task synchronously - for testing LiveData
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        val application: Application = ApplicationProvider.getApplicationContext()
        val sharedPrefs = application.getSharedPreferences("TEST", Context.MODE_PRIVATE)
        fakeRepository = FakeDataRepository()
        viewModel = SearchViewModel(application, fakeRepository, PrefsManager(sharedPrefs))
    }

    @Test
    fun performSearch_validQuery_completesWithResults() {
        // GIVEN: a valid search term
        //          set fake success
        //          set fake results for a valid search
        fakeRepository.run {
            fakeDefinitionsSuccess = true
            fakeDefinitionsResults = listOf(
                Definition(definition = "Test definition 1", thumbs_up = 100, thumbs_down = 50),
                Definition(definition = "Test definition 2", thumbs_up = 50, thumbs_down = 100),
                Definition(definition = "Test definition 3", thumbs_up = 10, thumbs_down = 5)
            )
        }

        // WHEN: performSearch() is called
        val query = "VALID"
        viewModel.performSearch(query)

        // THEN: LiveData is updated with values indicating success
        //         currentCompletedSearch emits current query entered
        //         searchResults emits a non-empty list of Definitions, with
        //         searchStatus emits a value of SearchStatus.COMPLETED_WITH_RESULTS
        val currentCompletedSearch = viewModel.currentCompletedSearch.getOrAwaitValue()
        val searchResults = viewModel.searchResults.getOrAwaitValue()
        val searchStatus = viewModel.searchStatus.getOrAwaitValue()

        assertThat(currentCompletedSearch, `is`(query))
        assertThat(searchResults.size, `is`(3))
        assertThat(searchStatus, `is`(SearchStatus.COMPLETED_WITH_RESULTS))
    }
}