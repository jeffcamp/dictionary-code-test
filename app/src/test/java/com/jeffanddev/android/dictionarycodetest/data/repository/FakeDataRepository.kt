package com.jeffanddev.android.dictionarycodetest.data.repository

import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import com.jeffanddev.android.dictionarycodetest.data.model.WrappedDefinitions

class FakeDataRepository : DataRepository {

    var fakeDefinitionsSuccess = true
    var fakeDefinitionsResults: List<Definition> = emptyList()

    override suspend fun getDefinitions(term: String): WrappedDefinitions {
        fakeDefinitionsResults.map {
            it.word = term
            it
        }

        return WrappedDefinitions(fakeDefinitionsSuccess, fakeDefinitionsResults)
    }

    override suspend fun clearHistory() {
        // TODO: not implemented
    }
}