package com.jeffanddev.android.dictionarycodetest.data.preferences

import android.content.SharedPreferences

const val SHARED_PREFS_KEY = "com.jeffanddev.android.dictionarycodetest.PREFS"
const val PREF_SEARCH_FILTER_THUMBS_UP = 0
const val PREF_SEARCH_FILTER_THUMBS_DOWN = 1

private const val PREF_SEARCH_FILTER = "PREF_SEARCH_FILTER"

class PrefsManager(private val sharedPreferences: SharedPreferences) {

    fun setSearchFilter(searchFilter: Int) {
        if (searchFilter == PREF_SEARCH_FILTER_THUMBS_UP || searchFilter == PREF_SEARCH_FILTER_THUMBS_DOWN) {
            sharedPreferences.edit().putInt(PREF_SEARCH_FILTER, searchFilter).apply()
        }
    }

    fun getSearchFilter(): Int {
        return sharedPreferences.getInt(PREF_SEARCH_FILTER, PREF_SEARCH_FILTER_THUMBS_UP)
    }
}