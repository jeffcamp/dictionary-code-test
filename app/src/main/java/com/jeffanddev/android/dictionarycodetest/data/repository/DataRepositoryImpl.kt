package com.jeffanddev.android.dictionarycodetest.data.repository

import android.content.Context
import com.jeffanddev.android.dictionarycodetest.data.model.WrappedDefinitions
import com.jeffanddev.android.dictionarycodetest.data.source.DataSource
import com.jeffanddev.android.dictionarycodetest.data.source.LocalDataSource
import com.jeffanddev.android.dictionarycodetest.ext.isConnectedToInternet
import java.util.*

class DataRepositoryImpl(private val context: Context, private val remoteDataSource: DataSource,
                         private val localDataSource: LocalDataSource) : DataRepository {

    // Note: Query term will be stored as lower case in database, since API results can include a mix of
    // lower case and Capitalized values for the word, and we want to store search terms consistently in database.
    override suspend fun getDefinitions(term: String): WrappedDefinitions {
        val lowerCaseTerm = term.toLowerCase(Locale.getDefault())
        return if (context.isConnectedToInternet) {
            val remoteDefinitions = remoteDataSource.getDefinitions(lowerCaseTerm)
            if (remoteDefinitions.success) {
                // need to store as lower case so all results can be retrieved from database
                val definitionsWithLowerCaseTerm = remoteDefinitions.definitions.map {
                    it.word = lowerCaseTerm
                    it
                }
                localDataSource.updateDefinitions(lowerCaseTerm, definitionsWithLowerCaseTerm) // update cache
                WrappedDefinitions(true, definitionsWithLowerCaseTerm)
            } else {
                localDataSource.getDefinitions(lowerCaseTerm)
            }
        } else {
            localDataSource.getDefinitions(lowerCaseTerm)
        }
    }

    override suspend fun clearHistory() {
        localDataSource.clearHistory()
    }
}