package com.jeffanddev.android.dictionarycodetest.provider

import android.content.SearchRecentSuggestionsProvider

class SearchSuggestionsProvider : SearchRecentSuggestionsProvider() {
    init {
        setupSuggestions(AUTHORITY, DATABASE_MODE_QUERIES)
    }

    companion object {
        const val AUTHORITY = "com.jeffanddev.android.dictionarycodetest.provider.SearchSuggestionsProvider"
        const val MODE: Int = DATABASE_MODE_QUERIES
    }
}