package com.jeffanddev.android.dictionarycodetest.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jeffanddev.android.dictionarycodetest.R
import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import com.jeffanddev.android.dictionarycodetest.databinding.ItemDefinitionHeaderBinding
import com.jeffanddev.android.dictionarycodetest.databinding.ItemDefintionBinding

private const val POSITION_HEADER = 0
private const val VIEW_TYPE_HEADER = 0
private const val VIEW_TYPE_DEFINITION = 1

class DefinitionsRecyclerViewAdapter : ListAdapter<DataItem, RecyclerView.ViewHolder>(DefinitionsDiffCallback()) {

    var searchTerm = ""

    private val nbrOfResults: Int
        get() = itemCount - 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_HEADER) {
            HeaderViewHolder.from(parent)
        } else {
            DefinitionViewHolder.from(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dataItem = getItem(position)
        when (holder) {
            is HeaderViewHolder -> {
                if (dataItem is DataItem.HeaderItem) {
                    holder.bind(dataItem.searchTerm, nbrOfResults)
                }
            }
            is DefinitionViewHolder -> {
                if (dataItem is DataItem.DefinitionItem) {
                    holder.bind(dataItem.definition)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == POSITION_HEADER) VIEW_TYPE_HEADER else VIEW_TYPE_DEFINITION
    }

    fun addHeaderAndSubmitList(searchResults: List<Definition>) {
        submitList(listOf(DataItem.HeaderItem(searchTerm)) + searchResults.map { DataItem.DefinitionItem(it) })

        // ensure any previous results are cleared
        if (searchResults.isEmpty()) notifyDataSetChanged()
    }
}

private class HeaderViewHolder private constructor(private val binding: ItemDefinitionHeaderBinding)
    : RecyclerView.ViewHolder(binding.root) {

    fun bind(searchTerm: String, nbrOfResults: Int) {
        if (nbrOfResults > 0) {
            binding.searchText = itemView.context.getString(R.string.s__parenthesis_d, searchTerm, nbrOfResults)
        } else {
            binding.searchText = searchTerm
        }
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemDefinitionHeaderBinding.inflate(layoutInflater, parent, false)
            return HeaderViewHolder(binding)
        }
    }
}

private class DefinitionViewHolder private constructor(private val binding: ItemDefintionBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(definition: Definition) {
        binding.definition = definition
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): RecyclerView.ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemDefintionBinding.inflate(layoutInflater, parent, false)
            return DefinitionViewHolder(binding)
        }
    }
}

class DefinitionsDiffCallback: DiffUtil.ItemCallback<DataItem>() {

    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem === newItem
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }
}

sealed class DataItem {
    data class DefinitionItem(val definition: Definition) : DataItem() {
        override val id = definition.hashCode()
    }

    data class HeaderItem(val searchTerm: String) : DataItem() {
        override val id = searchTerm.hashCode()
    }

    abstract val id: Int
}