package com.jeffanddev.android.dictionarycodetest.data.repository

import com.jeffanddev.android.dictionarycodetest.data.model.WrappedDefinitions

interface DataRepository {
    suspend fun getDefinitions(term: String): WrappedDefinitions
    suspend fun clearHistory()
}