package com.jeffanddev.android.dictionarycodetest.data.source

import com.jeffanddev.android.dictionarycodetest.data.model.WrappedDefinitions

interface DataSource {
    suspend fun getDefinitions(term: String): WrappedDefinitions
}