package com.jeffanddev.android.dictionarycodetest.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.jeffanddev.android.dictionarycodetest.data.model.Definition

@Dao
interface DictionaryDao {

    @Insert
    fun insert(definition: Definition)

    @Query("SELECT * FROM definition_table WHERE word == :word")
    fun getDefinitions(word: String): List<Definition>

    @Query("DELETE FROM definition_table WHERE word == :word")
    fun removeDefinitions(word: String)

    @Query("DELETE FROM definition_table")
    fun clear()
}