package com.jeffanddev.android.dictionarycodetest.data.model

data class WrappedDefinitions(val success: Boolean, val definitions: List<Definition>)