package com.jeffanddev.android.dictionarycodetest.search

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.jeffanddev.android.dictionarycodetest.R
import com.jeffanddev.android.dictionarycodetest.data.preferences.PREF_SEARCH_FILTER_THUMBS_DOWN
import com.jeffanddev.android.dictionarycodetest.data.preferences.PREF_SEARCH_FILTER_THUMBS_UP
import com.jeffanddev.android.dictionarycodetest.data.preferences.PrefsManager
import com.jeffanddev.android.dictionarycodetest.ext.appComponent
import javax.inject.Inject

class SearchFilterDialogFragment : DialogFragment() {
    @Inject lateinit var prefsManager: PrefsManager

    var listener: Listener? = null

    interface Listener {
        fun onFilterSelection(selection: Int)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        requireContext().appComponent.inject(this)

        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.sort_by)
        val view = View.inflate(activity, R.layout.dialog_search_filter, null)
        builder.setView(view)
        setupView(view)
        return builder.create()
    }

    override fun onPause() {
        super.onPause()
        dismiss()
    }

    private fun setupView(view: View) {
        val radioGroup = view.findViewById<RadioGroup>(R.id.searchFilterRadioGroup)

        // set initial filter preference
        when (prefsManager.getSearchFilter()) {
            PREF_SEARCH_FILTER_THUMBS_UP -> radioGroup.check(R.id.thumbsUpRadioButton)
            PREF_SEARCH_FILTER_THUMBS_DOWN -> radioGroup.check(R.id.thumbsDownRadioButton)
        }

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            val filterSelection = when (checkedId) {
                R.id.thumbsDownRadioButton -> PREF_SEARCH_FILTER_THUMBS_DOWN
                else -> PREF_SEARCH_FILTER_THUMBS_UP
            }
            prefsManager.setSearchFilter(filterSelection)
            listener?.onFilterSelection(filterSelection)
            dismiss()
        }
    }

    companion object {
        const val TAG = "SearchFilterDialogFragment"
    }
}