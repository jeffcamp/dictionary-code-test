package com.jeffanddev.android.dictionarycodetest.search

import android.app.Application
import android.provider.SearchRecentSuggestions
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import com.jeffanddev.android.dictionarycodetest.data.preferences.PREF_SEARCH_FILTER_THUMBS_DOWN
import com.jeffanddev.android.dictionarycodetest.data.preferences.PrefsManager
import com.jeffanddev.android.dictionarycodetest.data.repository.DataRepository
import com.jeffanddev.android.dictionarycodetest.provider.SearchSuggestionsProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class SearchStatus { NONE, IN_PROGRESS, COMPLETED_WITH_RESULTS, COMPLETED_NO_RESULTS, FAILED }

class SearchViewModel(val app: Application, private val repository: DataRepository, private val prefsManager: PrefsManager)
    : AndroidViewModel(app) {

    private var filterSelection: Int

    private val _searchStatus = MutableLiveData<SearchStatus>()
    val searchStatus: LiveData<SearchStatus>
        get() = _searchStatus

    private val _currentCompletedSearch = MutableLiveData<String>()
    val currentCompletedSearch: LiveData<String>
        get() = _currentCompletedSearch

    private val _searchResults = MutableLiveData<List<Definition>>()
    val searchResults: LiveData<List<Definition>>
        get() = _searchResults

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        _searchStatus.value = SearchStatus.NONE
        _currentCompletedSearch.value = ""
        _searchResults.value = emptyList()
        filterSelection = prefsManager.getSearchFilter()
    }

    override fun onCleared() {
        super.onCleared()

        viewModelJob.cancel()
    }

    fun performSearch(query: String) {
        val trimmedQuery = query.trim() // trim any leading or trailing whitespace from query text
        handleSearchStarted()
        coroutineScope.launch {
            val wrappedDefinitions = repository.getDefinitions(trimmedQuery)
            if (wrappedDefinitions.success) {
                val sortedDefinitions = getSortedDefinitions(wrappedDefinitions.definitions)
                _currentCompletedSearch.value = trimmedQuery
                _searchResults.value = sortedDefinitions
                if (sortedDefinitions.isNotEmpty()) {
                    _searchStatus.value = SearchStatus.COMPLETED_WITH_RESULTS
                } else {
                    _searchStatus.value = SearchStatus.COMPLETED_NO_RESULTS
                }
                saveRecentSearch(trimmedQuery)
            } else {
                _searchStatus.value = SearchStatus.FAILED
            }
        }
    }

    fun clearHistory() {
        coroutineScope.launch {
            SearchRecentSuggestions(app, SearchSuggestionsProvider.AUTHORITY, SearchSuggestionsProvider.MODE)
                .clearHistory()
            repository.clearHistory()
        }
    }

    fun updateFilter(selection: Int) {
        if (selection != filterSelection) {
            filterSelection = selection
            prefsManager.setSearchFilter(filterSelection)

            searchResults.value?.let {
                _searchResults.value = getSortedDefinitions(it)
            }
        }
    }

    private fun handleSearchStarted() {
        _currentCompletedSearch.value = ""
        _searchResults.value = emptyList()
        _searchStatus.value = SearchStatus.IN_PROGRESS
    }

    private fun saveRecentSearch(query: String) {
        SearchRecentSuggestions(app, SearchSuggestionsProvider.AUTHORITY, SearchSuggestionsProvider.MODE)
            .saveRecentQuery(query, null)
    }

    private fun getSortedDefinitions(definitions: List<Definition>): List<Definition> {
        return when (filterSelection) {
            PREF_SEARCH_FILTER_THUMBS_DOWN -> definitions.sortedByDescending { it.thumbs_down }
            else -> definitions.sortedByDescending { it.thumbs_up }
        }
    }
}
