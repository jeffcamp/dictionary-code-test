package com.jeffanddev.android.dictionarycodetest.dependency

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.jeffanddev.android.dictionarycodetest.data.preferences.PrefsManager
import com.jeffanddev.android.dictionarycodetest.data.preferences.SHARED_PREFS_KEY
import com.jeffanddev.android.dictionarycodetest.data.repository.DataRepository
import com.jeffanddev.android.dictionarycodetest.data.repository.DataRepositoryImpl
import com.jeffanddev.android.dictionarycodetest.data.source.LocalDataSource
import com.jeffanddev.android.dictionarycodetest.data.source.RemoteDataSource
import com.jeffanddev.android.dictionarycodetest.database.DictionaryDao
import com.jeffanddev.android.dictionarycodetest.database.DictionaryDatabase
import com.jeffanddev.android.dictionarycodetest.database.DictionaryDatabase.Companion.DATABASE_NAME
import com.jeffanddev.android.dictionarycodetest.network.NetworkService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideDictionaryDatabase(context: Context): DictionaryDatabase {
        return Room.databaseBuilder(context, DictionaryDatabase::class.java, DATABASE_NAME).build()
    }

    @Provides
    @Singleton
    fun provideDictionaryDao(database: DictionaryDatabase): DictionaryDao {
        return database.dictionaryDao
    }

    @Provides
    @Singleton
    fun provideLocalDataSource(dictionaryDao: DictionaryDao): LocalDataSource {
        return LocalDataSource(dictionaryDao)
    }

    @Provides
    @Singleton
    fun provideRemoteDataSource(networkService: NetworkService): RemoteDataSource {
        return RemoteDataSource(networkService)
    }

    @Provides
    @Singleton
    fun provideDataRepository(context: Context, remoteDataSource: RemoteDataSource, localDataSource: LocalDataSource)
            : DataRepository {
        return DataRepositoryImpl(context, remoteDataSource, localDataSource)
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(SHARED_PREFS_KEY, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun providePrefsManager(sharedPreferences: SharedPreferences): PrefsManager {
        return PrefsManager(sharedPreferences)
    }
}