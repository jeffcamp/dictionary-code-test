package com.jeffanddev.android.dictionarycodetest.dependency

import android.content.Context
import com.jeffanddev.android.dictionarycodetest.AppObject
import com.jeffanddev.android.dictionarycodetest.data.preferences.PrefsManager
import com.jeffanddev.android.dictionarycodetest.data.repository.DataRepository
import com.jeffanddev.android.dictionarycodetest.search.SearchViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: AppObject) {

    @Provides
    @Singleton
    fun provideApplication(): AppObject {
        return app
    }

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return app.applicationContext
    }

    @Provides
    @Singleton
    fun provideSearchViewModelFactory(app: AppObject, repository: DataRepository, prefsManager: PrefsManager)
            : SearchViewModelFactory {
        return SearchViewModelFactory(app, repository, prefsManager)
    }
}