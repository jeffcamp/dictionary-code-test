package com.jeffanddev.android.dictionarycodetest.data.model

data class DefinitionsResponse(val list: List<Definition>)