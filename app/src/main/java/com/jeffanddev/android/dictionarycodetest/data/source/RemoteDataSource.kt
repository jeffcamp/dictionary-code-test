package com.jeffanddev.android.dictionarycodetest.data.source

import com.jeffanddev.android.dictionarycodetest.data.model.WrappedDefinitions
import com.jeffanddev.android.dictionarycodetest.network.NetworkService

class RemoteDataSource(private val networkService: NetworkService) : DataSource {

    override suspend fun getDefinitions(term: String): WrappedDefinitions {
        return try {
            val definitionsAsync = networkService.getDefinitionsAsync(term).await()
            WrappedDefinitions(true, definitionsAsync.list)
        } catch (t: Throwable) {
            WrappedDefinitions(false, emptyList())
        }
    }
}