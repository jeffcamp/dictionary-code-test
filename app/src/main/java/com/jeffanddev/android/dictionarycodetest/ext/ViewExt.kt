package com.jeffanddev.android.dictionarycodetest.ext

import android.view.View
import android.view.ViewGroup

fun View.enableViewAndChildren(isEnabled: Boolean) {
    setEnabled(isEnabled)
    if (this is ViewGroup) {
        for (i in 0 until childCount) {
            getChildAt(i).enableViewAndChildren(isEnabled)
        }
    }
}