package com.jeffanddev.android.dictionarycodetest.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.jeffanddev.android.dictionarycodetest.data.model.Definition


@Database(entities = [Definition::class], version = 1, exportSchema = false)
abstract class DictionaryDatabase : RoomDatabase() {
    abstract val dictionaryDao: DictionaryDao

    companion object {
        const val DATABASE_NAME = "dictionary_database"
    }
}