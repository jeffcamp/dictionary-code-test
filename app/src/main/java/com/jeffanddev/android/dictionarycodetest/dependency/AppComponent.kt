package com.jeffanddev.android.dictionarycodetest.dependency

import com.jeffanddev.android.dictionarycodetest.search.SearchFilterDialogFragment
import com.jeffanddev.android.dictionarycodetest.search.SearchFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, DataModule::class])
interface AppComponent {
    fun inject(searchFragment: SearchFragment)
    fun inject(searchFilterDialogFragment: SearchFilterDialogFragment)
}