package com.jeffanddev.android.dictionarycodetest.search

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jeffanddev.android.dictionarycodetest.data.preferences.PrefsManager
import com.jeffanddev.android.dictionarycodetest.data.repository.DataRepository

@Suppress("UNCHECKED_CAST")
class SearchViewModelFactory(private val application: Application,
                             private val repository: DataRepository,
                             private val prefsManager: PrefsManager) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>) = SearchViewModel(application, repository, prefsManager) as T
}