package com.jeffanddev.android.dictionarycodetest.data.source

import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import com.jeffanddev.android.dictionarycodetest.data.model.WrappedDefinitions
import com.jeffanddev.android.dictionarycodetest.database.DictionaryDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalDataSource(private val dictionaryDao: DictionaryDao) : DataSource {

    override suspend fun getDefinitions(term: String): WrappedDefinitions {
        var definitions: List<Definition> = emptyList()
        withContext(Dispatchers.IO) {
            definitions = dictionaryDao.getDefinitions(term)
        }
        return if (definitions.isNotEmpty()) {
            WrappedDefinitions(true, definitions)
        } else {
            WrappedDefinitions(false, definitions)
        }
    }

    suspend fun updateDefinitions(term: String, definitions: List<Definition>) {
        withContext(Dispatchers.IO) {
            dictionaryDao.removeDefinitions(term) // clear any previous cached definitions

            definitions.forEach {
                dictionaryDao.insert(it)
            }
        }
    }

    suspend fun clearHistory() {
        withContext(Dispatchers.IO) {
            dictionaryDao.clear()
        }
    }
}