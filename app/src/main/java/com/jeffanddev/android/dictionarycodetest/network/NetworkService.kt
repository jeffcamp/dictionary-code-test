package com.jeffanddev.android.dictionarycodetest.network

import com.jeffanddev.android.dictionarycodetest.data.model.DefinitionsResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {

    @GET("define")
    fun getDefinitionsAsync(@Query("term") type: String): Deferred<DefinitionsResponse>
}