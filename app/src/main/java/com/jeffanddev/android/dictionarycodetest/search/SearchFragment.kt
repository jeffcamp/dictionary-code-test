package com.jeffanddev.android.dictionarycodetest.search

import android.app.SearchManager
import android.content.ComponentName
import android.os.Bundle
import android.text.InputFilter
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jeffanddev.android.dictionarycodetest.MainActivity
import com.jeffanddev.android.dictionarycodetest.R
import com.jeffanddev.android.dictionarycodetest.databinding.FragmentSearchBinding
import com.jeffanddev.android.dictionarycodetest.ext.appComponent
import com.jeffanddev.android.dictionarycodetest.ext.hideKeyboard
import javax.inject.Inject

private const val TEXT_MAX_LENGTH = 100

class SearchFragment : Fragment(), SearchFilterDialogFragment.Listener {

    @Inject lateinit var viewModelFactory: SearchViewModelFactory

    private lateinit var binding: FragmentSearchBinding
    private lateinit var viewModel: SearchViewModel

    private var filterResultsMenuItem: MenuItem? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchBinding.inflate(inflater)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.context.appComponent.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
        binding.viewModel = viewModel
        binding.definitionsList.adapter = DefinitionsRecyclerViewAdapter()

        setupSearch()
        setupObservers()
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        filterResultsMenuItem = menu.findItem(R.id.actionFilterResults)
        filterResultsMenuItem?.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionClearCache -> viewModel.clearHistory()
            R.id.actionFilterResults -> showFilterDialog()
        }

        return true
    }

    private fun setupSearch() {
        context?.let { safeContext ->
            val searchManager = getSystemService(safeContext, SearchManager::class.java) as SearchManager
            binding.searchView.run {
                // Assumes current activity is the searchable activity
                setSearchableInfo(searchManager.getSearchableInfo(ComponentName(safeContext, MainActivity::class.java)))
                setIconifiedByDefault(false)
                setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        query?.let { viewModel.performSearch(it) }
                        return true
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        return false
                    }
                })
                setOnSuggestionListener(object: SearchView.OnSuggestionListener {
                    override fun onSuggestionSelect(position: Int): Boolean {
                        return false
                    }

                    override fun onSuggestionClick(position: Int): Boolean {
                        suggestionsAdapter.cursor.run {
                            moveToPosition(position)
                            getString(getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1))?.let { suggestionText ->
                                viewModel.performSearch(suggestionText)
                            }
                        }

                        return true
                    }
                })
                findViewById<TextView>(R.id.search_src_text)?.let { searchViewEditText ->
                    searchViewEditText.filters = arrayOf(InputFilter.LengthFilter(TEXT_MAX_LENGTH))
                }
            }
        }
    }

    private fun setupObservers() {
        viewModel.searchStatus.observe(this, Observer { searchStatus ->
            if (searchStatus == SearchStatus.IN_PROGRESS) activity?.hideKeyboard()
        })

        viewModel.searchResults.observe(this, Observer { searchResults ->
            filterResultsMenuItem?.isVisible = searchResults.isNotEmpty()
        })
    }

    private fun showFilterDialog() {
        activity?.let { safeActivity ->
            val filterDialog = SearchFilterDialogFragment()
            filterDialog.listener = this
            filterDialog.show(safeActivity.supportFragmentManager, SearchFilterDialogFragment.TAG)
        }
    }

    override fun onFilterSelection(selection: Int) {
        viewModel.updateFilter(selection)
    }
}
