package com.jeffanddev.android.dictionarycodetest.search

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import com.jeffanddev.android.dictionarycodetest.ext.enableViewAndChildren

@BindingAdapter("showIfSearchInProgress")
fun showIfSearchInProgress(view: View, searchStatus: SearchStatus) {
    view.isVisible = searchStatus == SearchStatus.IN_PROGRESS
}

@BindingAdapter("showIfSearchCompleted")
fun showIfSearchCompleted(view: View, searchStatus: SearchStatus) {
    view.isVisible = searchStatus == SearchStatus.COMPLETED_WITH_RESULTS || searchStatus == SearchStatus.COMPLETED_NO_RESULTS
}

@BindingAdapter("showIfSearchCompletedNoResults")
fun showIfSearchCompletedNoResults(view: View, searchStatus: SearchStatus) {
    view.isVisible = searchStatus == SearchStatus.COMPLETED_NO_RESULTS
}

@BindingAdapter("showIfSearchFailed")
fun showIfSearchFailed(view: View, searchStatus: SearchStatus) {
    view.isVisible = searchStatus == SearchStatus.FAILED
}

@BindingAdapter("showUntilSearchPerformed")
fun showUntilSearchPerformed(view: View, searchStatus: SearchStatus) {
    view.isVisible = searchStatus == SearchStatus.NONE
}

@BindingAdapter("disableViewIfSearchInProgress")
fun disableViewIfSearchInProgress(view: View, searchStatus: SearchStatus) {
    if (searchStatus == SearchStatus.IN_PROGRESS) {
        view.enableViewAndChildren(false)
    } else {
        view.enableViewAndChildren(true)
    }
}

@BindingAdapter("handleSearchViewCompletedSearch")
fun handleSearchViewCompletedSearch(searchView: SearchView, searchStatus: SearchStatus) {
    if (searchStatus == SearchStatus.COMPLETED_WITH_RESULTS || searchStatus == SearchStatus.COMPLETED_NO_RESULTS) {
        searchView.run {
            setQuery("", false)
            clearFocus()
        }
    }
}

@BindingAdapter("bindDefinitions")
fun bindDefinitions(recyclerView: RecyclerView, data: List<Definition>) {
    val adapter = recyclerView.adapter as DefinitionsRecyclerViewAdapter
    adapter.addHeaderAndSubmitList(data)
}

@BindingAdapter("bindCurrentSearch")
fun bindCurrentSearch(recyclerView: RecyclerView, currentSearch: String) {
    val adapter = recyclerView.adapter as DefinitionsRecyclerViewAdapter
    adapter.searchTerm = currentSearch
}
