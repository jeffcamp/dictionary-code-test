package com.jeffanddev.android.dictionarycodetest

import android.app.Application
import com.jeffanddev.android.dictionarycodetest.dependency.AppModule
import com.jeffanddev.android.dictionarycodetest.dependency.DaggerAppComponent
import com.jeffanddev.android.dictionarycodetest.dependency.DataModule
import com.jeffanddev.android.dictionarycodetest.dependency.NetworkModule

class AppObject : Application() {
    val appComponent = DaggerAppComponent.builder()
        .appModule(AppModule(this)).networkModule(NetworkModule()).dataModule(DataModule()).build()
}