package com.jeffanddev.android.dictionarycodetest.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "definition_table")
data class Definition(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name = "word")
    var word: String = "",

    @ColumnInfo(name = "definition")
    val definition: String = "",

    @ColumnInfo(name = "thumbs_up")
    val thumbs_up: Int = 0,

    @ColumnInfo(name = "thumbs_down")
    val thumbs_down: Int = 0
) {

    val thumbsDownString: String
        get() = thumbs_down.toString()

    val thumbsUpString: String
        get() = thumbs_up.toString()
}