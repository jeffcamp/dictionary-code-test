package com.jeffanddev.android.dictionarycodetest.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.jeffanddev.android.dictionarycodetest.data.model.Definition
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

private const val CAT = "cat"

@RunWith(AndroidJUnit4::class)
class DictionaryDatabaseTest {
    private lateinit var dictionaryDao: DictionaryDao
    private lateinit var database: DictionaryDatabase

    @Before
    fun createDatabase() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        // Use In Memory database that is gone after the tests are completed
        database = Room.inMemoryDatabaseBuilder(context, DictionaryDatabase::class.java)
            .allowMainThreadQueries()
            .build()

        dictionaryDao = database.dictionaryDao
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    @Test
    fun definitionDao_performAvailableOperations_Success() {
        // GIVEN: several Definitions
        val insertedDefinition0 = Definition(word = CAT, definition = "meow", thumbs_up = 100, thumbs_down = 10)
        val insertedDefinition1 = Definition(word = CAT, definition = "the cat's meow", thumbs_up = 50, thumbs_down = 5)
        val insertedDefinition2 = Definition(word = CAT, definition = "a fluffy pet", thumbs_up = 25, thumbs_down = 10)

        // WHEN: insert() is called
        dictionaryDao.insert(insertedDefinition0)
        dictionaryDao.insert(insertedDefinition1)
        dictionaryDao.insert(insertedDefinition2)

        // THEN: Definitions were inserted and retrieved with getDefinitions()
        val fetchedDefinitions = dictionaryDao.getDefinitions(CAT)
        assertDefinitionsMatch(fetchedDefinitions[0], insertedDefinition0)
        assertDefinitionsMatch(fetchedDefinitions[1], insertedDefinition1)
        assertDefinitionsMatch(fetchedDefinitions[2], insertedDefinition2)
        assertThat(fetchedDefinitions.size, `is`(3))


        // GIVEN: database contains 3 Definitions

        // WHEN: clear() is called
        dictionaryDao.clear()

        // THEN: Definitions were cleared and getDefinitions() returns an empty list
        val fetchedEmptyList = dictionaryDao.getDefinitions(CAT)
        assertThat(fetchedEmptyList, `is`(emptyList()))
    }

    private fun assertDefinitionsMatch(actual: Definition, expected: Definition) {
        assertThat(actual.word, `is`(expected.word))
        assertThat(actual.definition, `is`(expected.definition))
        assertThat(actual.thumbs_up, `is`(expected.thumbs_up))
        assertThat(actual.thumbs_down, `is`(expected.thumbs_down))
    }
}